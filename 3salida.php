<?php
    $cajas=[
        "Parrafo 1",
        "Parrafo 2",
        "Parrafo 3",
    ];
    
    $etiquetas=[
        "codigo",
        "modelo",
        "marca",
    ];
    
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            for($c=0;$c<count($cajas);$c++){
                echo "<div>$cajas[$c]</div>";
            }
        ?>
        
        <form method="get">
            <?php
                for($i=0;$i<count($etiquetas);$i++){
                // principio bucle para imprimir los controles del formulario
            ?>
                <label for="<?= $etiquetas[$i] ?>"><?= $etiquetas[$i] ?></label>
                <input type="text" id="<?= $etiquetas[$i] ?>" name="<?= $etiquetas[$i] ?>"/>
            <?php
                // fin del bucle
                }
            ?>
            <button>Enviar</button>
        </form>
        
        
    </body>
</html>
