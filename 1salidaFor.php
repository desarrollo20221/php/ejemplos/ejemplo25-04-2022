<?php
    $variable = [
        "valores" => [
            "santander",
            "laredo",
            "potes"
            ],
        "indice" => [0,23,45],
    ];
    define("BOTON", "ENVIAR");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="get">
            <select name="poblaciones">
                <?php
                    for ($c=0 ;$c<3 ;$c++){
                ?>
                    <option value="<?= $variable["indice"][$c] ?>"><?= $variable["valores"][$c] ?></option>
                <?php
                    }
                ?>                
            </select>
            <button><?= BOTON ?></button>
        </form>
        <?php
            
        ?>
    </body>
</html>
