<?php
    $variable = [
        "valores" => [
            "santander",
            "laredo",
            "potes"
            ],
        "indice" => [0,23,45],
    ];
    define("BOTON", "ENVIAR");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="get">
            <select name="poblaciones">
                <?php 
                    echo '<option value="' . $variable["indice"][0] . '">' . $variable["valores"][0] . '</option>';                  
                ?>
                <option value="<?= $variable["indice"][1] ?>"><?= $variable["valores"][1] ?></option>
                <option value="<?= $variable["indice"][2] ?>"><?= $variable["valores"][2] ?></option>
            </select>
            <button><?= BOTON ?></button>
        </form>
        <?php
            
        ?>
    </body>
</html>
