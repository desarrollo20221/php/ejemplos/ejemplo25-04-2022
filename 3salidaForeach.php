<?php
    $cajas=[
        "Parrafo 1",
        "Parrafo 2",
        "Parrafo 3",
    ];
    
    $etiquetas=[
        "codigo",
        "modelo",
        "marca",
    ];
    
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            foreach ($cajas as $value){
                echo "<div>$value</div>";
            }
        ?>
        
        <form method="get">
            <?php
                    foreach ($etiquetas as $i=>$value) {
                // principio bucle para imprimir los controles del formulario
            ?>
                <label for="<?= $etiquetas[$i] ?>"><?= $etiquetas[$i] ?></label>
                <input type="text" id="<?= $etiquetas[$i] ?>" name="<?= $etiquetas[$i] ?>"/>
            <?php
                // fin del bucle
                }
            ?>
            <button>Enviar</button>
        </form>
        
        
    </body>
</html>
